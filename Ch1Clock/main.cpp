#include <iostream>
#include <string>
#include "clockType.h"

using namespace std;

void main()
{
	clockType theClock(11);
	//clockType myclock = new clockType();
	theClock.printTime();

	cout << endl;

	int theHour, theMin, theSec;

	theClock.getTime(theHour, theMin, theSec);
	cout << "Hour: " << theHour << endl;
	cout << "Min: " << theMin << endl;
	cout << "Sec: " << theSec << endl;


}